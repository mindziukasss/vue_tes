import MoviesList from "../../components/MoviesList"

export default [
    {
        path: '/movies',
        name: 'MoviesList',
        component: MoviesList
    }
]