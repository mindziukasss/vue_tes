import Vue from 'vue'
import Router from 'vue-router'
import moviesList from './movie/movie'


Vue.use(Router)

export default new Router({
    routes: [
        ...moviesList,
    ]
})