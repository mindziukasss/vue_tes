import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import store from './store/index'
import router from './route/route'
import axios from 'axios'

Vue.use(BootstrapVue)

Vue.config.productionTip = false

Vue.component('modal', {
  template: '#modal-template'
})

axios.defaults.baseURL = 'http://localhost:8000';
import Dashboard from './components/Dashboard.vue';

new Vue({
  el: "#app",
  router,
  store,
  render: h => h(Dashboard)
}).$mount('#app')