import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

import movie from './modules/movie/movie'

Vue.use(Vuex, axios)

export default new Vuex.Store( {
    modules: {
        movie
    }
})