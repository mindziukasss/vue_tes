import axios from 'axios'

const state = {
    getMovies: {}
}

const getters = {
    getMovies: state => state.getMovies,

}

const actions = {
    async getMovies ({commit}) {
        axios.get('http://localhost:8000/movies')
            .then(data => {
                let movies = data.data
                commit('getMovies', movies)
            })
    }
}

const mutations = {
    getMovies (state, getMovies) {
        state.getMovies = getMovies
    }

}


export default {
    actions,
    state,
    getters,
    mutations
}